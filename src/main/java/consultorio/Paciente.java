package Consultorio;

public class Paciente extends Persona{

    private String domicilio;
    private String medicoCabecera;
    private String telefono;

    public Paciente(String nombre, String apellido, String dni, String domicilio, String medicoCabecera, String telefono){

        super(nombre, apellido, dni);

        this.domicilio = domicilio;
        this.medicoCabecera = medicoCabecera;
        this.telefono = telefono;
    }



    //Getters de Persona
    public String getNombre(){
        return super.getNombre();
    }
    public String getApellido(){
        return super.getApellido();
    }
    public String getDni(){
        return super.getDni();
    }
    //Getters propios de la clase
    public String getMedicoCabecera(){
        return medicoCabecera;
    }
    public String getTelefono(){
        return telefono;
    }
    public String getDomicilio(){
        return domicilio;
    }
}
