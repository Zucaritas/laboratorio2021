package Consultorio;

public class Turno{

    public String nombrePaciente;
    public String apellidoPaciente;
    
    public Turno(String apellidoPaciente, String nombrePaciente){
        
        this.apellidoPaciente = apellidoPaciente;
        this.nombrePaciente = nombrePaciente;

    }

    public String getNombrePaciente(){
        return nombrePaciente;
    }
    public String getApellidoPaciente(){
        return apellidoPaciente;
    }

}
