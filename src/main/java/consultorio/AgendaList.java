package Consultorio;

import java.util.ArrayList;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.*;

public class AgendaList implements Agenda {

    Medico medicoPropietario;       //De esta forma cada medico posee su propia agenda vinvulada a sus datos
    LocalDate fechaCreacion;        //Marca el inicio de la agenda y despues se usa para mantener actualizada la fecha de expiracion
    LocalDate fechaExpiracion;      //Marca el final de cada mes para crear una nueva agenda para el siguiente
    ArrayList<Turno> listaTurnos;   //Lista de turnos en el corriente mes

    public AgendaList(Medico medicoPropietario){
        this.medicoPropietario = medicoPropietario;
        fechaCreacion = LocalDate.now();
        fechaExpiracion = fechaCreacion.with(lastDayOfMonth()); //Asigna el ultimo dia del mes, para la renovacion la Agenda
        ArrayList<Turno> listaTurnos = new ArrayList<Turno>(24);
    }
    
    @Override
    public void nuevoTurno(String apellidoPaciente, String nombrePaciente, int duracion, LocalDate fecha) { 
        
        Turno nuevoTurno = new Turno(apellidoPaciente, nombrePaciente);

        verificarTurnoDia(duracion, fecha);
        listaTurnos.add(nuevoTurno);


    }

    @Override
    public Boolean verificarTurnoDia(int cantidad, LocalDate fecha) {

        for (Turno turno : listaTurnos) {
        }
        return null;
    }

    @Override
    public Turno turnosDisponibles(LocalDate Fecha, ArrayList<Turno> listaTurnos) {
        return null;
    }
    
}
