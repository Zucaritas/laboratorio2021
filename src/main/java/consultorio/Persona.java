package Consultorio;
abstract class Persona {

    public String nombre;
    public String apellido;
    public String dni;
    public String domicilio;

    public Persona (String nombre, String apellido, String dni){
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
    }

    public Persona (String nombre, String apellido, String dni, String domicilio){
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.domicilio = domicilio;
    }

    //Getters
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getDni(){
        return dni;
    }
}
