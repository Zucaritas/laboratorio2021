package Consultorio;
public class Medico extends Persona {
        
    private String numeroMatricula;
    
    //public Agenda agendaUnica; //Cada Medico pueda gestionar su propia agenda, pero agenda pide fecha de inicio y fecha de fin

    
    public Medico(String nombre, String apellido, String dni, String domicilio, String numeroMatricula){
                  
        super(nombre, apellido, dni, domicilio);
        this.numeroMatricula = numeroMatricula;
    }

   //getter
   public String getNumeroMatricula(){
       return numeroMatricula;
   }
}
