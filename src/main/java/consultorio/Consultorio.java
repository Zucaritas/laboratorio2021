package Consultorio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

public class Consultorio {

    public String nombreDeConsultorio;
    public String telefono;
    public String direccion; 

    public LocalDateTime horaApertura; //Problemas para mandar horarios de apertura y cierre en el formato adecuado
    public LocalDateTime horaCierre;

    //Coleccion de listas
    ArrayList<Medico> listaMedicos;
    ArrayList<Paciente> listaPacientes;

    //Metodo Constructor
    public Consultorio(String nombreDelConsultorio, String telefono, String direccion/*, LocalDateTime horaApertura, LocalDateTime horaCierre*/){
        
        this.nombreDeConsultorio = nombreDelConsultorio;
        this.telefono = telefono;
        this.direccion = direccion;
     //   this.horaApertura = horaApertura;
     //   this.horaCierre = horaCierre;
    
        listaMedicos = new ArrayList<Medico>();
        listaPacientes = new ArrayList<Paciente>();
    }

    //Metodos Medicos

    public void nuevoMedico(String nombre, String apellido, String dni, String domicilio, String numeroMatricula){
        Boolean existe;
        Medico nuevoMedico = new Medico(nombre, apellido, dni, domicilio, numeroMatricula);

        existe = existeMedico(dni);
        if (existe == false){
            agregarMedico(nuevoMedico);
        }
    }

    public void agregarMedico(Medico medico){
        listaMedicos.add(medico);
    }

    public boolean existeMedico(String dni){
        boolean respuesta = false;
        for (Medico medico:listaMedicos){
            if (dni == medico.getDni()){
                respuesta = true;
            }
        }
        return respuesta;
    }

    public Medico buscarMedico(String dni){
        Medico respuesta= null;

        for (Medico medico:listaMedicos){
            if (dni == medico.getDni()){
                respuesta = medico;
            }
        }
        return respuesta;
    }
   
    public int cantidadMedicos(){
        return listaMedicos.size();
    }

    //Metodos Pacientes

    public void nuevoPaciente(String nombre, String apellido, String dni, String domicilio, String medicoCabecera, String telefono){
        boolean existe;
        Paciente nuevoPaciente = new Paciente(nombre, apellido, dni, domicilio, medicoCabecera, telefono);

        existe = existePaciente(dni);
        if (existe == false){
            agregarPaciente(nuevoPaciente);
        }       
    }

    public boolean existePaciente(String dni){
        boolean respuesta = false;
        for (Paciente paciente:listaPacientes){
            if (dni == paciente.getDni()){
                respuesta = true;
            }
        }
        return respuesta;
    }

    public void agregarPaciente(Paciente paciente){
        listaPacientes.add(paciente);
    }

    public int cantidadPacientes(){
        return listaPacientes.size();
    }
}
