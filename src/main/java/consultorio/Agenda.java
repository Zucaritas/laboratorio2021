package Consultorio;

import java.util.ArrayList;
import java.time.LocalDate;

interface Agenda {

    //Metodos Relacionados a Turnos
    void nuevoTurno(String apellidoPaciente, String nombrePaciente, int duracion, LocalDate fecha);
    Boolean verificarTurnoDia(int cantidad, LocalDate fecha);
    Turno turnosDisponibles(LocalDate Fecha, ArrayList<Turno>listaTurnos);

}
