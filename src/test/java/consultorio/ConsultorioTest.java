package consultorio;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import Consultorio.Consultorio;
import Consultorio.Medico;

public class ConsultorioTest {
    
    Consultorio consultorioPruebas = new Consultorio("Laboratorio De Pruebas", "telefono", "direccion");
    Medico medicoPrueba = new Medico("Juan", "Pruebas", "11.111.111", "domicilio", "numeroMatricula");


    //Metodos Medico
    @Test
    public void nuevoMedicoTest(){
        consultorioPruebas.nuevoMedico("Pedro", "Apellido", "22.222.222", "domicilio", "numeroMatricula");
        assertEquals(true, consultorioPruebas.existeMedico("22.222.222"));
    }

    @Test
    public void agregarMedicoTest(){  
       consultorioPruebas.agregarMedico(medicoPrueba);
       assertEquals(true, consultorioPruebas.existeMedico("11.111.111"));
    }

    @Test
    public void buscarMedicoTest(){
        consultorioPruebas.agregarMedico(medicoPrueba);
        assertEquals(medicoPrueba, consultorioPruebas.buscarMedico("11.111.111"));
    }

    @Test
    public void cantidadMedicosTest(){
        consultorioPruebas.agregarMedico(medicoPrueba);
        assertEquals(1, consultorioPruebas.cantidadMedicos());
    }

    //Metodos Paciente


}
